# FuzzyNNRuntimeReduction
Runtime Reduction in Fuzzy Neural Network by reducing number of rules using an optimization approach

In this project we decreased number of rules in fuzzy neural network (FNN) by solving an optimization problem.
this task is preformed for Regression and Classification (Binary /Multi class) problems. we used pulp library for the optimization problem task in Linear Programming (LP).
in our approach we have absolute operation in the optimization function. but in LP we can't use absolute of a variable, so we redefine it as follow:

 ![GitHub Logo](https://github.com/AmirmohammadRostami/FuzzyNNRuntimeReduction/blob/master/eq1.png)

## Optimization functions:

* ### Regression:
 we have N training samples, so we write the optimization function as :
 
![GitLab Logo](https://gitlab.com/reflax/FuzzyNNRuntimeReduction/raw/ede4f5a2859f176f251e16a8598935e905bb74a8/eq2.png)

that

![GitLab Logo](https://gitlab.com/reflax/FuzzyNNRuntimeReduction/raw/ede4f5a2859f176f251e16a8598935e905bb74a8/eq3.png) 

 and
 
![GitLab Logo](https://gitlab.com/reflax/FuzzyNNRuntimeReduction/raw/ede4f5a2859f176f251e16a8598935e905bb74a8/eq4.png)

* ### Binary Classification:

![GitLab Logo](https://gitlab.com/reflax/FuzzyNNRuntimeReduction/raw/ede4f5a2859f176f251e16a8598935e905bb74a8/eq5.png)

* ### and MultiClass Classification:

![GitLab Logo](https://gitlab.com/reflax/FuzzyNNRuntimeReduction/raw/ede4f5a2859f176f251e16a8598935e905bb74a8/eq6.png)

that

![GitLab Logo](https://gitlab.com/reflax/FuzzyNNRuntimeReduction/raw/ede4f5a2859f176f251e16a8598935e905bb74a8/eq7.png)


> special thanks to my teammate, @EmadArezumand

>Don’t hesitate to contact me for any further information or details, amirmohammadrostami@yahoo.com
